{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "Sprite3",
  "bbox_bottom": 63,
  "bbox_left": 1,
  "bbox_right": 53,
  "bbox_top": 1,
  "bboxMode": 0,
  "collisionKind": 1,
  "collisionTolerance": 0,
  "DynamicTexturePage": false,
  "edgeFiltering": false,
  "For3D": false,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"4d11275f-cbe3-46da-bb64-4afd5bf5910b",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"2d519b9b-5d05-4d29-955b-e7166a481af5",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"c138112d-e16a-47cf-8f0b-1410ef8e3f85",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"864b7e8a-72a7-4eac-aef3-4f38b803151e",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"45db2999-6a3a-4834-b187-68d82bd75085",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"1abf05ee-9dd1-4dba-a868-b71fb1a363e0",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"90abd783-b5ec-4e05-aeb7-b05c4167e456",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"34023dda-8c42-4342-933c-1341e0505fe2",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"35acebaa-f479-4ea2-8ec0-25e48c8caa26",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"f753abfc-985f-44bb-8dd4-19bf65f15bbe",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"00306ef1-d88a-468c-a5a0-485cdc1ea86b",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"c8b5e084-2003-4609-83af-546b0960e901",},
  ],
  "gridX": 0,
  "gridY": 0,
  "height": 64,
  "HTile": false,
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"54df7084-8d07-4984-99e1-aa750fb78d9a","blendMode":0,"displayName":"default","isLocked":false,"opacity":100.0,"visible":true,},
  ],
  "nineSlice": null,
  "origin": 0,
  "parent": {
    "name": "スプライト",
    "path": "folders/スプライト.yy",
  },
  "preMultiplyAlpha": false,
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "Sprite3",
    "autoRecord": true,
    "backdropHeight": 768,
    "backdropImageOpacity": 0.5,
    "backdropImagePath": "",
    "backdropWidth": 1366,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "events": {"resourceType":"KeyframeStore<MessageEventKeyframe>","resourceVersion":"1.0","Keyframes":[],},
    "eventStubScript": null,
    "eventToFunction": {},
    "length": 12.0,
    "lockOrigin": false,
    "moments": {"resourceType":"KeyframeStore<MomentsEventKeyframe>","resourceVersion":"1.0","Keyframes":[],},
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "showBackdrop": true,
    "showBackdropImage": false,
    "timeUnits": 1,
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"1.0","Keyframes":[
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"4d11275f-cbe3-46da-bb64-4afd5bf5910b","path":"sprites/Sprite3/Sprite3.yy",},},},"Disabled":false,"id":"66f5d4d9-9a8d-401b-9566-06bd96173ec2","IsCreationKey":false,"Key":0.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"2d519b9b-5d05-4d29-955b-e7166a481af5","path":"sprites/Sprite3/Sprite3.yy",},},},"Disabled":false,"id":"801ec57a-36e2-48f5-9003-7bf44c100ddf","IsCreationKey":false,"Key":1.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"c138112d-e16a-47cf-8f0b-1410ef8e3f85","path":"sprites/Sprite3/Sprite3.yy",},},},"Disabled":false,"id":"66baf4f6-5dac-41a8-aa31-d8ebdcfcd99a","IsCreationKey":false,"Key":2.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"864b7e8a-72a7-4eac-aef3-4f38b803151e","path":"sprites/Sprite3/Sprite3.yy",},},},"Disabled":false,"id":"cfb928aa-81ae-437a-a59e-769cad74fb94","IsCreationKey":false,"Key":3.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"45db2999-6a3a-4834-b187-68d82bd75085","path":"sprites/Sprite3/Sprite3.yy",},},},"Disabled":false,"id":"8c92e7ef-d581-4224-94e9-59daf1156bba","IsCreationKey":false,"Key":4.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"1abf05ee-9dd1-4dba-a868-b71fb1a363e0","path":"sprites/Sprite3/Sprite3.yy",},},},"Disabled":false,"id":"504f2ae4-8283-4901-b6c5-ca284bba800c","IsCreationKey":false,"Key":5.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"90abd783-b5ec-4e05-aeb7-b05c4167e456","path":"sprites/Sprite3/Sprite3.yy",},},},"Disabled":false,"id":"d00a10f2-7b50-45bb-9226-f06f6def03fa","IsCreationKey":false,"Key":6.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"34023dda-8c42-4342-933c-1341e0505fe2","path":"sprites/Sprite3/Sprite3.yy",},},},"Disabled":false,"id":"ffbfe45e-09c2-4273-9e06-3cb10ea701d6","IsCreationKey":false,"Key":7.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"35acebaa-f479-4ea2-8ec0-25e48c8caa26","path":"sprites/Sprite3/Sprite3.yy",},},},"Disabled":false,"id":"be1736f1-01b8-4871-9d07-0f24c633e7df","IsCreationKey":false,"Key":8.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"f753abfc-985f-44bb-8dd4-19bf65f15bbe","path":"sprites/Sprite3/Sprite3.yy",},},},"Disabled":false,"id":"97c0a56e-4174-4d3b-82ea-a495b6a29fc2","IsCreationKey":false,"Key":9.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"00306ef1-d88a-468c-a5a0-485cdc1ea86b","path":"sprites/Sprite3/Sprite3.yy",},},},"Disabled":false,"id":"d5511a60-5a06-4635-9366-b14d7198d356","IsCreationKey":false,"Key":10.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"c8b5e084-2003-4609-83af-546b0960e901","path":"sprites/Sprite3/Sprite3.yy",},},},"Disabled":false,"id":"63185065-5c7e-47d6-9f5c-0f8c7332bbe4","IsCreationKey":false,"Key":11.0,"Length":1.0,"Stretch":false,},
          ],},"modifiers":[],"spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange": null,
    "volume": 1.0,
    "xorigin": 0,
    "yorigin": 0,
  },
  "swatchColours": null,
  "swfPrecision": 2.525,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "type": 0,
  "VTile": false,
  "width": 64,
}