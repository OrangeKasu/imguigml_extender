/// @description imguigmlを使用するための管理オブジェクト
#macro imguigml_layer_name "imguigml_extender_layer"
if(debug_mode == false){
	show_debug_message("The \"imguigml_manager\" object will be destroyed because debug_mode is set to false.")
	instance_destroy();
	return ;
}
// ImGuiを初期化
ImGui.__Initialize()
/**
 * メニュー構造体
 * @param {string} _name - メニューの名前
 * @param {any} _window - ウィンドウクラス
 */
function MenuParam(_name, _window) constructor {
	menu_name = _name;
	window = _window;
}
// メニュー初期化
menu_array = [
	new MenuParam("Show layer menu", new LayerTreeImGui("layer_tree_menu")),
	new MenuParam("Show instance menu", new ObjectAssetsWindow("object_assets_window")),
	new MenuParam("Show room menu", new RoomAssetsWindow("room_assets_window")),
	new MenuParam("Show audio menu", new AudioImGui("audio_assets_window")),
	new MenuParam("Show global variable menu", new GlobalVariableWindow("global_variable_window")),
];
// データ管理map初期化
data_map = new ImGuiDataMap();
// ゲッター
GetDataMap = function(){
	return data_map;
}
// imguigml_managerは専用のレイヤーに移動する
var new_layer = layer_create(0,imguigml_layer_name);
var element_id = undefined;
var elements = layer_get_all_elements(layer);
for(var ele_i = 0; ele_i < array_length(elements); ele_i ++){
	var obj = layer_instance_get_instance(elements[ele_i]);
	if(obj.id == self.id){
		element_id = elements[ele_i];
		break;
	}
}
if(!is_undefined(element_id)){
	layer_element_move(element_id,new_layer);
}
