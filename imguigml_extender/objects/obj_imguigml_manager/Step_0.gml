/// @description デバッグウィンドウの更新処理
ImGui.BeginMainMenuBar();
// メニューの数を増やす場合はBeginMenuとEndMenuを使って増やす
if (ImGui.BeginMenu("Menu List")) {
	for(var menu_i = 0; menu_i < array_length(menu_array); menu_i ++){
		var menu = menu_array[menu_i];
		if (ImGui.MenuItem(menu.menu_name, undefined, undefined, !menu.window.is_open)){
			data_map.AddWindow(menu.window);
		} 
	}
	ImGui.EndMenu();
}
// メニューを更新する
data_map.UpdateMenu();