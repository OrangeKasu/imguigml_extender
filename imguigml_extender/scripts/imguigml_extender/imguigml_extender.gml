/**
 * ImGuiを使用してウィンドウの基本的な機能を提供する基底クラス
 * @class
 * @param {string} _name - ウィンドウの名前
 */
function ImGuiWindowBase(_name) constructor {
	// 適当に400/400で定義している
	static windowWidth = 400;
    static windowHeight = 400;

    name = _name;
    is_open = false;
    is_focused = false;
    is_collapsed = false;
    /**
     * ウィンドウを開く関数
     * @function
     */
    static OpenWindow = function(){
        is_open = true;
    }
    /**
     * ウィンドウを描画する関数。継承して使用。
     * @function
     */
    static DrawWindow = function(){
        
    }
    /**
     * ウィンドウを更新する関数
     * @function
     */
    static UpdateWindow = function(){
        if(is_open){
        	if(is_collapsed){
        		ImGui.SetNextWindowCollapsed(true, ImGuiCond.Always);
        	} else {
        		ImGui.SetNextWindowCollapsed(is_collapsed, ImGuiCond.Once);
        	}
        	
            // windowサイズを指定。
            ImGui.SetNextWindowSize(windowWidth, windowHeight, ImGuiCond.Once);

            var is_begin = ImGui.Begin(name, true, ImGuiWindowFlags.None, ImGuiReturnMask.Both);
            is_focused = ImGui.IsWindowFocused();
            is_open = is_begin & ImGuiReturnMask.Pointer;
            // is_beginの戻り値がtrueなら
            if(is_begin & ImGuiReturnMask.Return){
        		DrawWindow();
            }
        	// ImGui描画終了
            ImGui.End();
        }
    }
    
    /**
     * 情報構造体からテキスト情報を取得する関数
     * @function
     * @param {any} infoStruct - 情報構造体
     * @returns {string} - テキスト情報
     */
    static GetInfoText = function(infoStruct){
        var str = string(infoStruct);
        str = string_delete(str, 1,2);
        str = string_delete(str, string_length(str) - 2,2);
        str = string_replace_all(str,",",",\n");
        return str;
    }
    /**
	 * ウィンドウを描画するイベントを処理する関数。
	 * @param {string} name - ウィンドウの名前
	 */
    static DrawEndEvent = function(){
    	
    }
    /**
	 * ウィンドウが削除されたときに呼ばれる関数。
	 */
    static OnDelete = function(){
    	
    }
    /**
	 * 構造体を描画する関数。
	 * @param {any} _struct - 描画対象の構造体
	 */
    static DrawStructMenu = function(_struct){
    	var values = variable_struct_get_names(_struct);
    	for(var value_i = 0; value_i < array_length(values); value_i ++){
    		var value_name = values[value_i];
    		
    		var value = variable_struct_get(_struct, value_name);
	    		var rt = DrawVariableMenu(value_name, value);
	    		if(!is_undefined(rt)){
					variable_struct_set(_struct, value_name, rt);
			}
    	}
    }
    /**
	 * 変数からタイプを特定し、デバッグ機能を描画。
	 * @param {string} value_name - 変数の名前
	 * @param {any} value - 変数の値
	 * @returns {any} - デバッグ機能で変更後の値
	 */
    static DrawVariableMenu = function(value_name, value){
    	var rt = undefined;
    	if(ImGui.TreeNode(value_name)){
			if(is_real(value)){
				rt = ImGui.InputFloat(value_name, value, 1);
			} else if(is_array(value)){
				DreaArrayMenu(value_name, value);
			} else if(is_bool(value)){
				rt = ImGui.Checkbox(value_name, value);
			} else if(is_method(value) || is_callable(value)){
				if(ImGui.Button(string("Call Function : {0}() ", value_name))){
					// 引数必須の関数を呼ぶとエラー落ちする
					value();
				}
				if (ImGui.IsItemHovered()) {
					ImGui.BeginTooltip();
					ImGui.Text(string(value));
					ImGui.EndTooltip();
				}
			} else if(is_struct(value)){
				DrawStructMenu(value);
			} else {
				rt = ImGui.InputText(value_name, value);
			}
			ImGui.TreePop();
		}
		return rt;
    }
    /**
	 * 配列を描画する関数。
	 * @param {any} _array_name - 配列名
	 * @param {any} _array - 描画対象の配列
	 */
    static DreaArrayMenu = function(_array_name, _array){
    	for(var array_i = 0; array_i < array_length(_array); array_i ++){
    		var str = string("{0}[{1}]", _array_name, array_i);
    		var rt = DrawVariableMenu(str, _array[array_i]);
    		if(!is_undefined(rt)){
    			_array[array_i] = rt;
    		}
    	}
    }
}

/**
 * レイヤーに属するエレメント描画用の基底クラス
 * @class
 * @param {string} _name - ウィンドウの名前
 * @param {number} _layer_id - ウィンドウが属するレイヤーのID
 */
function ImGuiLayerInfoWindowBase(_name, _layer_id) : ImGuiWindowBase(_name) constructor {
    layer_id = _layer_id;
    
    open_focus_color = c_yellow;
    open_not_focus_color = c_red;
    
    
    /**
     * レイヤーの情報を描画する関数
     * @function
     */
    static DrawLayerInfo = function(){
        var _layer_x = layer_get_x(layer_id);
        _layer_x = ImGui.DragFloat("layer_x", _layer_x, room_width / 100, -room_width, room_width);
        layer_x(layer_id, _layer_x);
        
        var _layer_y = layer_get_y(layer_id);
        _layer_y = ImGui.DragFloat("layer_y", _layer_y, room_height / 100, -room_height, room_height);
        layer_y(layer_id, _layer_y);
        
        var _layer_hspeed = layer_get_hspeed(layer_id);
        _layer_hspeed = ImGui.DragFloat("layer_hspeed", _layer_hspeed, 0.01, -100, 100);
        layer_hspeed(layer_id, _layer_hspeed);
        
        var _layer_vspeed = layer_get_vspeed(layer_id);
        _layer_vspeed = ImGui.DragFloat("layer_vspeed", _layer_vspeed, 0.01, -100, 100);
        layer_vspeed(layer_id, _layer_vspeed);
    }
    
    /**
	 * フォーカス中のオブジェクトの矩形を描画する関数
	 * @function
	 * @param {number} _sprite_index - 対象のスプライトのインデックス
	 * @param {number} _draw_x - 描画座標の X 軸位置
	 * @param {number} _draw_y - 描画座標の Y 軸位置
	 * @param {number} _xscale - 描画座標の X 軸の拡大率
	 * @param {number} _yscale - 描画座標の Y 軸の拡大率
	 */
    static DrawFocusRectangle = function(_sprite_index, _draw_x, _draw_y, _xscale, _yscale){
    	if(!is_open){
            return ;
		}
		// オブジェクトの描画座標とサイズの計算
		var offset_x = sprite_get_xoffset(_sprite_index) * _xscale // abs(_xscale);
		var offset_y = sprite_get_yoffset(_sprite_index) * _yscale //abs(_yscale);
		var width = _draw_x + sprite_get_width(_sprite_index) * abs(_xscale) - offset_x;
		var height = _draw_y + sprite_get_height(_sprite_index) * abs(_yscale) - offset_y;
		var x_left_pos = _draw_x - offset_x;
		var x_right_pos = x_left_pos + sprite_get_width(_sprite_index) * _xscale;
		var y_top_pos = _draw_y - offset_y;
		var y_bottom_pos = y_top_pos + sprite_get_height(_sprite_index) * _yscale;
		
		// オブジェクトがフォーカスされているかによる描画色の設定
		if(is_focused){
		    draw_set_color(open_focus_color);
		} else {
		    draw_set_color(open_not_focus_color);
		}
		
		// オブジェクトの輪郭を描画
		draw_line_width(x_left_pos, y_top_pos, x_left_pos, y_bottom_pos, 2);
		draw_line_width(x_left_pos, y_top_pos, x_right_pos, y_top_pos, 2);
		draw_line_width(x_left_pos, y_bottom_pos, x_right_pos, y_bottom_pos, 2);
		draw_line_width(x_right_pos, y_top_pos, x_right_pos, y_bottom_pos, 2);
		
		// 描画色を白に戻す
		draw_set_color(c_white);
    }
    
    
}
/**
 * ImGuiのウィンドウ管理するクラス
 * @class
 * @constructor
 */
function ImGuiDataMap() constructor {
    data_map = new DebugWindowMap();
    /**
	 * ウィンドウを追加する関数
	 * @param {ImGuiWindowBase} window - 追加するウィンドウ
	 */
    static AddWindow = function(window){
    	window.OpenWindow();
    	data_map.Add(window.name, window);
    }
    /**
     * objectのメニューを追加する関数
     * @function
     * @param {any} obj - オブジェクトメニュー名
     * @param {string} name - オブジェクトの名前
     * @param {number} layer_id - オブジェクトが属するレイヤーのID
     */
    static AddObject = function(obj, name, layer_id){
        var objMenu = new ObjectImGui(name,layer_id, obj);
		objMenu.OpenWindow();
		data_map.Add(name, objMenu);
    }
    /**
     * tilesetのメニューを追加する関数
     * @function
     * @param {string} name - tilesetメニュー名
     * @param {number} layer_id - tilesetが属するレイヤーのID
     * @param {number} tile - タイルのID
     * @param {number} tileset - tilesetのID
     */
    static AddTileset = function(name, layer_id, tile, tileset){
    	var tileMenu = new TilesetImGui(name,layer_id, tile, tileset);
    	tileMenu.OpenWindow();
    	data_map.Add(name, tileMenu);
    }
    /**
     * backgroundのメニューを追加する関数
     * @function
     * @param {string} name - backgroundメニュー名
     * @param {number} layer_id - backgroundを追加する関数が属するレイヤーのID
     * @param {number} background - backgroundを追加する関数のID
     */
    static AddBackground = function(name, layer_id, background){
    	var backgroundMenu = new BackgroundImGui(name,layer_id, background);
    	backgroundMenu.OpenWindow();
    	data_map.Add(name, backgroundMenu);
    }
    /**
	 * spriteのメニューを追加する関数
	 * @function
	 * @param {string} name - spriteメニュー名
	 * @param {number} layer_id - レイヤーID
	 * @param {number} sprite_element_id - スプライトの要素ID
	 */
    static AddSprite = function(name, layer_id, sprite_element_id){
    	var spriteMenu = new SpriteImGui(name,layer_id, sprite_element_id);
    	spriteMenu.OpenWindow();
    	data_map.Add(name, spriteMenu);
    }
    /**
     * すべてのウィンドウを削除する関数
     * @function
     */
    static DeleatAll = function(){
    	data_map.Iterator( function(key,data){
    		data.OnDelete();
        	data.is_open = false;
        });
		data_map.Clear();
    }
    /**
     * 指定したキーのオブジェクトを取得する関数
     * @function
     * @param {string} key - キー
     * @returns {array} - オブジェクト
     */
    static GetObject = function(key){
    	return data_map.Get(key);
    }
    /**
     * メニューを更新する関数
     * @function
     */
    static UpdateMenu = function(){
    	data_map.Iterator(method({this: data_map} , function(key,data){
    		if(!is_undefined(data)){
    			if(data.is_open){
					data.UpdateWindow();
				} else {
					data.OnDelete();
					this.Delete(key);
				}
    		}
    	}));
    }
    /**
     * メニューを更新するための関数
     * @function
     * @param {string} key - キー
     * @param {any} data - オブジェクト
     */
    static UpdateMenuFunc = function(key,data){}
    /**
     * オブジェクトの矩形を描画する関数
     * @function
     */
    static DrawEndEvent = function(){
    	data_map.Iterator( function(key,data){
        	data.DrawEndEvent();
        });
    }
}