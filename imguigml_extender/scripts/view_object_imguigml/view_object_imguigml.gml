/**
 * @classdesc ImGuiを使用してオブジェクトの情報を表示するウィンドウを管理するクラス
 * @class
 * @extends ImGuiLayerInfoWindowBase
 * @param {string} _name - ウィンドウの名前
 * @param {number} _layer_id - ウィンドウの所属するレイヤーのID
 * @param {number} _object_index - 管理するオブジェクトのインデックス
 */
function ObjectImGui(_name,_layer_id, _object_index) : ImGuiLayerInfoWindowBase(_name, _layer_id) constructor {
    self_object =_object_index;
    sprite_surf = -1;
    is_destroy_popup = false;
    is_move = false;
    is_draw_bbox = false;
    sprite_view_scale = 1;
    variable_filter_text = "";
    /**
	 * ウィンドウを描画する関数
	 * @function
	 */
    static DrawWindow = function(){
        if(instance_exists(self_object) == false){
            ImGui.Text("object is not exists");
            return ;
        }
        if(is_focused){
            OnFocusedMove();
        }
        if(ImGui.BeginTabBar("object info tab")){
            if (ImGui.BeginTabItem("basic info")) {
            	DrawBasicInfo();
            	ImGui.EndTabItem();
            }
            if (ImGui.BeginTabItem("sprite info")) {
                DrawSpriteInfo();
            	ImGui.EndTabItem();
            }
            if (ImGui.BeginTabItem("speed_direction info")) {
                DrawSpeedDirection();
            	ImGui.EndTabItem();
            }
            if (ImGui.BeginTabItem("variable info")) {
                DrawVariable();
            	ImGui.EndTabItem();
            }
            ImGui.EndTabBar();
        }
        
		if(is_destroy_popup){
			DrawPopupModalWindow();
		}
    }
    /**
	 * DrawEndEventで呼ばれる関数
	 * @function
	 */
    static DrawEndEvent = function(){
    	if(instance_exists(self_object)){
			var spr = self_object.sprite_index;
			DrawFocusRectangle(spr, self_object.x,self_object.y,self_object.image_xscale,self_object.image_yscale);
		}
    }
    /**
	 * フォーカスされている場合の移動を処理する関数
	 * @function
	 */
    static OnFocusedMove = function(){
    	if(mouse_check_button_pressed(mb_middle)){
            var obj = collision_point(mouse_x,mouse_y,self_object,false,true);
            if(obj != noone){
                is_move = true;
            }
        }
        if(mouse_check_button_released(mb_middle)){
            is_move = false;
        }
        if(is_move){
            self_object.x = mouse_x;
            self_object.y = mouse_y;
        }
    }
    /**
	 * ポップアップモーダルウィンドウを描画する関数
	 * @function
	 */
    static DrawPopupModalWindow = function(){
    	var menu_str = "Caution!!";
		ImGui.OpenPopup(menu_str);
		ImGui.SetNextWindowPos(window_get_width() / 2, window_get_height () / 2, ImGuiCond.Appearing, 0.5, 0.5);
		if(ImGui.BeginPopupModal(menu_str,undefined, ImGuiWindowFlags.NoResize)){
			ImGui.Separator();
			ImGui.Text("Are you sure you want to destroy this instance?");
			if (ImGui.Button("yes")) {
				if(instance_exists(self_object)){
					with(self_object){
			            instance_destroy();
			        }
				}
			}
			ImGui.SameLine();
			if (ImGui.Button("no")) {
				ImGui.CloseCurrentPopup();
				is_destroy_popup = false;
			}
			ImGui.EndPopup();	
		}
    }
    /**
	 * オブジェクトの基本情報タブを描画する関数
	 * @function
	 */
    static DrawBasicInfo = function(){
        ImGui.Text(string("name: {0}", object_get_name(self_object.object_index)));
    	ImGui.Text(string("id: {0}", self_object.id));
    	ImGui.Text(string("pos: {0}", string("x: {0}, y: {1}", self_object.x, self_object.y)));
    	ImGui.Text(string("startPos: {0}", string("x: {0}, y: {1}", self_object.xstart, self_object.ystart)));
    	ImGui.Text(string("previousPos: {0}", string("x: {0}, y: {1}", self_object.xprevious, self_object.yprevious)));
    	self_object.solid = ImGui.Checkbox(string("solid"),self_object.solid);
    	self_object.persistent = ImGui.Checkbox(string("persistent"),self_object.persistent);
    	self_object.visible = ImGui.Checkbox(string("visible"), self_object.visible);
    	if(ImGui.Button("Reset Pos")){
    		self_object.x = self_object.xstart;
    		self_object.y = self_object.ystart;
    	}
    	if(ImGui.Button("Destroy")){
	        is_destroy_popup = true;
	    }
    }
    /**
	 * オブジェクトのスプライト情報タブを描画する関数
	 * @function
	 */
    static DrawSpriteInfo = function(){
    	if(self_object.sprite_index != -1){
    		DrawSpriteInfoText();
        	DrawSpriteParameter();
        	DrawSpriteImage();
    	} else {
    		ImGui.Text("sprite_index is undefined");
    	}
    }
    /**
	 * スプライト情報を描画する関数
	 * @function
	 */
    static DrawSpriteInfoText = function(){
    	if(ImGui.CollapsingHeader("sprite info text")){
    		var info = sprite_get_info(self_object.sprite_index);
    		var str = GetInfoText(info);
    		str = string_replace_all(str,"[","[\n");
    		ImGui.Text(str);
    	}
    }
    /**
	 * スプライトのパラメータを描画する関数
	 * @function
	 */
    static DrawSpriteParameter = function(){
    	if(ImGui.CollapsingHeader("sprite parameter")){
    		ImGui.Text(string("sprite_index: {0}", sprite_get_name(self_object.sprite_index)));
	        ImGui.Text(string("image_index: {0}", self_object.image_index));
	        if(ImGui.Button("Reset image_index")){
	            self_object.image_index = 0;
	        }
	        var spr_width = sprite_get_width(self_object.sprite_index);
	        var spr_height = sprite_get_height(self_object.sprite_index);
	    	ImGui.Text(string("sprite_width: {0}, sprite_height: {1}", spr_width, spr_height));
	    	var offset_array = [sprite_get_xoffset(self_object.sprite_index),sprite_get_yoffset(self_object.sprite_index)];
	    	var is_offset_change = ImGui.DragInt2("sprite_offset", offset_array,1,0,max(spr_width, spr_height));
	    	if(is_offset_change){
	    		sprite_set_offset(self_object.sprite_index, offset_array[0], offset_array[1]);
	    	}
	    	ImGui.SameLine();
	    	if(ImGui.Button("set center")){
	    		sprite_set_offset(self_object.sprite_index, spr_width / 2, spr_height / 2);
	    	}
	    	
	    	var scale_array = [self_object.image_xscale,self_object.image_yscale];
	    	var is_change = ImGui.DragFloat2("image_scale", scale_array,0.01,-10,10);
	    	if(is_change){
	    		self_object.image_xscale = scale_array[0];
	    		self_object.image_yscale = scale_array[1];
	    	}
	    	self_object.image_alpha = ImGui.DragFloat("image_alpha",self_object.image_alpha, 0.01,0,1);
	    	self_object.image_angle = ImGui.DragFloat("image_angle",self_object.image_angle, 1,0,360);
	    	self_object.image_speed = ImGui.DragFloat("image_speed",self_object.image_speed, 0.01,0,10);
    	}
    }
    /**
	 * オブジェクトのスプライトイメージを描画する関数
	 * @function
	 */
    static DrawSpriteImage = function(){
    	var xscale = (sprite_get_width(self_object.sprite_index) * abs(self_object.image_xscale)) * sprite_view_scale;
    	var yscale = (sprite_get_height(self_object.sprite_index) * abs(self_object.image_yscale)) * sprite_view_scale;
    	xscale = max(xscale, 1);
    	yscale = max(yscale, 1);
    	if (!surface_exists(sprite_surf)) {
        	sprite_surf = surface_create(xscale, yscale);
        	surface_set_target(sprite_surf);
        	DrawSprite();
        	surface_reset_target();
        }
    	ImGui.BeginChild("sprite_image", 0, 0, true);
        is_draw_bbox = ImGui.Checkbox("DrawBBox", is_draw_bbox);
        sprite_view_scale = ImGui.SliderFloat("view_scale", sprite_view_scale, 0,10);
        surface_resize(sprite_surf,xscale, yscale);
        surface_set_target(sprite_surf);
        draw_clear_alpha(c_white,1);
    	DrawSprite();
    	surface_reset_target();
        ImGui.Surface(sprite_surf);
        ImGui.EndChild();
    }
    /**
	 * オブジェクトの速度および方向情報タブを描画する関数
	 * @function
	 */
    static DrawSpeedDirection = function(){
        self_object.speed = ImGui.DragFloat("speed", self_object.speed, 0.01,-100,100);
        self_object.hspeed = ImGui.DragFloat("hspeed", self_object.hspeed, 0.01,-100,100);
        self_object.vspeed = ImGui.DragFloat("vspeed", self_object.vspeed, 0.01,-100,100);
        self_object.direction = ImGui.DragFloat("direction", self_object.direction, 1,-360,360);
        self_object.friction = ImGui.DragFloat("friction", self_object.friction, 0.01,0,100);
        self_object.gravity = ImGui.DragFloat("gravity", self_object.gravity, 0.01,-10,10);
        self_object.gravity_direction = ImGui.DragFloat("gravity_direction", self_object.gravity_direction, 0.01,0,100);
    }
    /**
	 * オブジェクトのスプライトを描画する関数
	 * @function
	 */
    static DrawSprite = function(){
    	var spr_xoffset = sprite_get_xoffset(self_object.sprite_index);
	    var spr_yoffset = sprite_get_yoffset(self_object.sprite_index);
    	sprite_set_offset(self_object.sprite_index, sprite_get_width(self_object.sprite_index) / 2, sprite_get_height(self_object.sprite_index) / 2)
    	draw_sprite_ext(
            self_object.sprite_index,
            self_object.image_index,
            sprite_get_xoffset(self_object.sprite_index) * abs(self_object.image_xscale) * sprite_view_scale,
            sprite_get_yoffset(self_object.sprite_index) * abs(self_object.image_yscale) * sprite_view_scale,
            self_object.image_xscale * sprite_view_scale,
            self_object.image_yscale * sprite_view_scale,
            self_object.image_angle,
            c_white,
            self_object.image_alpha
        );
        var posX = spr_xoffset * abs(self_object.image_xscale) * sprite_view_scale;
        var posY = spr_yoffset * abs(self_object.image_yscale) * sprite_view_scale;
        draw_circle_color(posX, posY,2,c_red,c_red,false);
        draw_line_color(0, posY, self_object.sprite_width * abs(self_object.image_xscale) * sprite_view_scale, posY, c_red,c_red);
        draw_line_color(posX, 0, posX, self_object.sprite_height * abs(self_object.image_yscale) * sprite_view_scale, c_red,c_red);
        sprite_set_offset(self_object.sprite_index, spr_xoffset, spr_yoffset);
    }
    /**
	 * 変数描画関数
	 * @function
	 */
    static DrawVariable = function(){
    	variable_filter_text = ImGui.InputText("filter", variable_filter_text);
    	
    	var values = variable_instance_get_names(self_object.id);
    	array_sort(values,function(a,b){
    		return string_compare(a,b);
    	});
    	for(var value_i = 0; value_i < array_length(values); value_i ++){
    		var value_name = values[value_i];
    		if(variable_filter_text != ""){
    			if(string_pos(variable_filter_text, value_name) == 0){
    				continue;
    			}
    		}
    		ImGui.Separator();
    		
    		var value = variable_instance_get(self_object.id, value_name);
    		var rt = DrawVariableMenu(value_name, value);
    		if(!is_undefined(rt)){
				variable_instance_set(self_object.id, value_name, rt);
			}
    	}
    }
}
/**
 * 受け取ったオブジェクトのマップを表示する関数
 * @function
 * @param {object} _objectMap - オブジェクトのマップ
 * @param {number} _layer_id - レイヤーのID
 */
function view_object_imguigml(_objectMap, _layer_id) {
    var itr = ds_map_find_first(_objectMap);
    var size = ds_map_size(_objectMap);
    for(var map_i = 0; map_i < size; map_i ++){
        if(ImGui.TreeNode(itr)){
            var array = ds_map_find_value(_objectMap, itr);
            for(var array_i = 0; array_i < array_length(array); array_i ++){
                var obj = array[array_i];
                var obj_name = object_get_name(obj.object_index);
                var name = string("{0}_{1}", obj_name,obj);
                if(ImGui.Button(name)){
                	if(instance_exists(obj_imguigml_manager)){
                		obj_imguigml_manager.GetDataMap().AddObject(obj, name, _layer_id);
                	}
                }
            }
            ImGui.TreePop();
        }
        itr = ds_map_find_next(_objectMap, itr);
    }
}
/**
 * オブジェクトをマップに追加する関数
 * @function
 * @param {number} _element - 追加するオブジェクトの要素
 * @param {object} _mapPtr - マップへのポインタ
 */
function add_object_map(_element, _mapPtr){
    var obj = layer_instance_get_instance(_element);
    if(instance_exists(obj)){
        var obj_name = object_get_name(obj.object_index);
		var is_regist = ds_map_exists(_mapPtr, obj_name);
		if(is_regist){
		    var array = ds_map_find_value(_mapPtr, obj_name);
		    array_push(array,obj);
		} else {
		    var array = array_create(0);
		    array_push(array,obj);
		    ds_map_add(_mapPtr, obj_name, array);
		}
    }
}