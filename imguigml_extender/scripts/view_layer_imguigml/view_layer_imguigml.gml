/**
 * LayerInfoImGui ImGui を使用して特定のレイヤーの情報を表示するためのウィンドウを管理するクラス。
 * @extends ImGuiLayerInfoWindowBase
 * @param {string} _name - ウィンドウの名前
 * @param {number} _layer_id - 対象のレイヤーの ID
 */
function LayerInfoImGui(_name, _layer_id) : ImGuiLayerInfoWindowBase(_name, _layer_id) constructor {
	is_destroy_popup = false;
	/**
	 * ウィンドウを描画する関数
	 * @function
	 */
	static DrawWindow = function(){
		if(!layer_exists(layer_id)){
			ImGui.Text("layer is not exists");
			return ;
		}
        DrawLayerInfo();
        ImGui.Text(string("depth: {0}", layer_get_depth(layer_id)));
        var is_visible = layer_get_visible(layer_id);
        var check = ImGui.Checkbox("visible", is_visible);
        layer_set_visible(layer_id, check);
        if(ImGui.Button("Destroy")){
	        is_destroy_popup = true;
	    }
        var fx = layer_get_fx(layer_id);
        if(fx != -1){
        	if(ImGui.CollapsingHeader("fx menu")){
        		var param_names = fx_get_parameter_names(fx);
				for (var param_i = 0; param_i < array_length(param_names); param_i ++) {
				    var name = param_names[param_i];
				    var value = fx_get_parameter(fx, name);
				    var rt = DrawVariableMenu(name, value);
				    if(!is_undefined(rt)){
				    	fx_set_parameter(fx, name, rt);
				    }
				}
        	}
        }
        if(is_destroy_popup){
			DrawPopupModalWindow();
		}
    }
    
    /**
	 * ポップアップモーダルウィンドウを描画する関数
	 * @function
	 */
    static DrawPopupModalWindow = function(){
    	var menu_str = "Caution!!";
		ImGui.OpenPopup(menu_str);
		ImGui.SetNextWindowPos(window_get_width() / 2, window_get_height () / 2, ImGuiCond.Appearing, 0.5, 0.5);
		if(ImGui.BeginPopupModal(menu_str,undefined, ImGuiWindowFlags.NoResize)){
			ImGui.Separator();
			ImGui.Text("Are you sure you want to destroy this layer?");
			if (ImGui.Button("yes")) {
				if(layer_exists(layer_id)){
					layer_destroy(layer_id);
				}
				is_destroy_popup = false;
			}
			ImGui.SameLine();
			if (ImGui.Button("no")) {
				ImGui.CloseCurrentPopup();
				is_destroy_popup = false;
			}
			ImGui.EndPopup();	
		}
    }
}
/**
 * LayerTreeImGui は ImGui を使用してレイヤーツリーを表示するためのウィンドウを管理するクラス
 * @extends ImGuiWindowBase
 * @param {string} _name - ウィンドウの名前
 */

function LayerTreeImGui(_name) : ImGuiWindowBase(_name) constructor {
	focus_name = "";
	/**
	 * ウィンドウを描画する関数
	 * @function
	 */
	static DrawWindow = function(){
		ProcessMouseClick();
		if(focus_name != ""){
			ImGui.SetWindowFocus(focus_name);
		}
		DrawLayerTreeWindow();
	}
	/**
	 * レイヤーツリーウィンドウを描画する関数
	 * @function
	 */
	static DrawLayerTreeWindow = function(){
		var all_layer = layer_get_all();
    
	    for(var layer_i = 0; layer_i < array_length(all_layer); layer_i ++){
	        var layer_id = all_layer[layer_i];
	        var all_layer_elements = layer_get_all_elements(layer_id);
	        var layer_name = layer_get_name(layer_id);
	        if(layer_name == imguigml_layer_name){
	        	continue;
	        }
	        if (ImGui.TreeNode(string(layer_name))) {
	        	if(ImGui.Button(string("show {0} layer debug window", layer_name))){
	        		var layerMenu = new LayerInfoImGui(layer_name, layer_id);
	        		obj_imguigml_manager.GetDataMap().AddWindow(layerMenu);
	        	}
	        	var object_map = ds_map_create();
	        	var sprite_map = ds_map_create();
	            for(var element_i = 0; element_i < array_length(all_layer_elements); element_i ++){
	                var element = all_layer_elements[element_i];
	                var layer_type = layer_get_element_type(element);
	                if(layer_type == layerelementtype_instance){
	                	add_object_map(element, object_map);
	                } else if(layer_type == layerelementtype_tilemap){
	                	view_tileset_imguigml(layer_id);
	                } else if(layer_type == layerelementtype_background){
	                	view_background_imguigml(layer_id);
	                } else {
	                	add_sprite_map(element, sprite_map);
	                }
	            }
	            if(ds_map_size(object_map) > 0){
	            	view_object_imguigml(object_map, layer_id);
	            }
	            if(ds_map_size(sprite_map) > 0){
	            	view_sprite_imguigml(sprite_map, layer_id);
	            }
				ImGui.TreePop();
			}
	    }
	}
	
	/**
	 * マウスクリック処理
	 */
	static ProcessMouseClick = function () {
		// releasedのタイミングじゃないとクリック時のfocusとかちあってしまう
	    if (!mouse_check_button_released(mb_left)) {
	        return;
	    }
		// 変数の初期化
	    focus_name = "";
	    var is_control_key = keyboard_check(vk_control);
	    var _list = ds_list_create();
	    var num = collision_point_list(mouse_x, mouse_y, all, false, true, _list, false);
		// クリック位置にオブジェクトがある場合に処理を続行
	    if (num > 0) {
	        HandleMouseClick(_list, is_control_key);
	    }
	}
	/**
	 * マウスクリックの対象処理。
	 * @param {array} _list - 衝突したオブジェクトのリスト
	 * @param {boolean} is_control_key - Ctrlキーが押されているかどうか
	 */
	static HandleMouseClick = function(_list, is_control_key) {
		// オブジェクトの数だけ繰り返し処理を行う
	    for (var i = 0; i < ds_list_size(_list); i++) {
	        var obj = _list[| i];
	        var name = string("{0}_{1}", object_get_name(obj.object_index), obj);
			// Ctrlキーが押されている場合とそうでない場合で処理を分岐
	        if (is_control_key) {
	        	// 配列に追加
	            obj_imguigml_manager.GetDataMap().AddObject(obj, name);
	        } else {
	        	// フォーカスする
	            HandleNonControlClick(name);
	        }
	    }
	}
	/**
	 * Ctrlキーが押されていない場合のマウスクリックの対象処理。
	 * @param {string} name - オブジェクトの名前
	 */
	static HandleNonControlClick = function(name) {
	    var data = obj_imguigml_manager.GetDataMap().GetObject(name);
	    if (!is_undefined(data) && data.is_open) {
	        focus_name = name;
	    }
	}
	
	
}