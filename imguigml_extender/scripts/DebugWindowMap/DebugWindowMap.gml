/**
 * デバッグ用のデータを管理するためのマップ拡張クラス
 * @class
 * @constructor
 */
function DebugWindowMap() constructor {
    map = ds_map_create();

    /**
     * キーとデータのペアを追加するメソッド
     * @param {any} key - マップに追加するキー
     * @param {any} data - マップに追加するデータ
     * @returns {boolean} - 追加が成功した場合はtrue、失敗した場合はfalse
     */
    Add = function(key, data){
        return ds_map_add(map, key, data);
    }

    /**
     * 指定されたキーに対応するデータを取得するメソッド
     * @param {any} key - 取得するデータのキー
     * @returns {any} - キーに対応するデータ
     */
    Get = function(key){
        return ds_map_find_value(map, key);
    }

    /**
     * 指定されたキーに対応するデータを削除するメソッド
     * @param {any} key - 削除するデータのキー
     * @returns {boolean} - 削除が成功した場合はtrue、失敗した場合はfalse
     */
    Delete = function(key){
        return ds_map_delete(map, key);
    }

    /**
     * マップ内の全てのキーとデータを削除するメソッド
     */
    Clear = function(){
        ds_map_clear(map);
    }

    /**
     * 指定されたキーがマップ内に存在するかどうかを判定するメソッド
     * @param {any} key - 存在を判定するキー
     * @returns {boolean} - キーが存在する場合はtrue、存在しない場合はfalse
     */
    IsExist = function(key){
        return ds_map_exists(map, key);
    }

    /**
     * マップの要素数を取得するメソッド
     * @returns {number} - マップの要素数
     */
    GetSize = function(){
        return ds_map_size(map);
    }

    /**
     * マップ内の全ての要素に対して指定された関数を実行するメソッド
     * @param {function} func - 各要素に対して実行される関数
     */
    Iterator = function(func){
        var itr = ds_map_find_first(map);
        var size = ds_map_size(map);
        for(var map_i = 0; map_i < size; map_i ++){
            var data = ds_map_find_value(map, itr);
            func(itr, data);
            itr = ds_map_find_next(map, itr);
        }
    }
}
