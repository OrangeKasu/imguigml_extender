/**
 * GlobalVariableWindow グローバル変数を管理し、ウィンドウとして表示するためのクラス
 * @class
 * @extends ImGuiWindowBase
 * @param {string} _name - ウィンドウの名前
 */
function GlobalVariableWindow(_name) : ImGuiWindowBase(_name) constructor {
	filter_text = "";
	/**
     * ウィンドウを描画する関数
     * @function
     */
    static DrawWindow = function(){
    	filter_text = ImGui.InputText("filter text", filter_text);
        var values = variable_instance_get_names(global);
        for(var value_i = 0; value_i < array_length(values); value_i ++){
    		var value_name = values[value_i];
    		if(filter_text != ""){
    			if(string_pos(filter_text, value_name) == 0){
        			continue;
        		}
    		}
    		var value = variable_global_get(value_name);
    		var rt = DrawVariableMenu(string("global.{0}", value_name), value);
    		if(!is_undefined(rt)){
				variable_global_set(value_name, rt);
			}
    	}
    }
    /**
     * グローバル変数のメニューを描画する関数
     * @function
     * @param {string} value_name - 変数の名前
     * @param {any} value - 変数の値
     * @returns {any} - 変更された変数の値
     */
    static DrawVariableMenu = function(value_name, value){
    	var rt = undefined;
    	// 関数を表示すると情報量が大きくなりすぎるのでスキップする
    	if(is_callable(value)){
    	    if(!is_numeric(value)){
    	        return rt;
    	    }
		} 
		
    	if(ImGui.TreeNode(value_name)){
			if(is_real(value)){
				rt = ImGui.InputFloat(value_name, value, 1);
			} else if(is_array(value)){
				DreaArrayMenu(value_name, value);
			} else if(is_bool(value)){
				rt = ImGui.Checkbox(value_name, value);
			} else if(is_struct(value)){
				DrawStructMenu(value);
			} else {
				rt = ImGui.InputText(value_name, value);
			}
			ImGui.TreePop();
		}
		ImGui.Separator();
		return rt;
    }
}
