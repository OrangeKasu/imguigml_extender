/**
 * ImGuiを使用して音声関連の情報を表示するウィンドウを管理するクラス
 * @class
 * @extends ImGuiWindowBase
 * @param {string} _name - ウィンドウの名前
 */
function AudioImGui(_name) : ImGuiWindowBase(_name) constructor {
    listener_index = 0;
    master_gain = audio_get_master_gain(0);
    is_debug = true;
    current_select_index = 0;
    audio_data = ds_map_create();
    name_array = array_create(0);
    filter_text = "";
    /**
     * サウンド再生時のパラメータ
     * @type {object}
     */
    playParam = {
        priotiry: 0,
        gain: master_gain,
        offset: 0,
        pitch: 1,
    }
    // オーディオデータの初期化
    var count = 0;
    while(audio_exists(count)){
        var name = audio_get_name(count);
        var param = new SoundParam(count);
        ds_map_add(audio_data,name,param);
        array_push(name_array, name);
        count ++;
    }
    /**
	 * ウィンドウを描画する関数
	 * @function
	 */
    static function DrawWindow(){
        DrawMasterMenu();
        if(ImGui.CollapsingHeader("sound assets")){
            DrawAssetsMenu();
        }
    }
    /**
	 * ウィンドウ削除時の処理を行う関数
	 * @function
	 */
    static function OnDelete(){
        audio_debug(false);
    }
    /**
	 * audio_master関係を描画する関数
	 * @function
	 */
    static DrawMasterMenu = function(){
        // マスターゲインの調整
        master_gain = ImGui.SliderFloat("Master gain", master_gain,0,1);
        audio_master_gain(master_gain);
        // リスナーインデックスの入力
        ImGui.InputInt("lisnter_index", listener_index,1,1);
        // マスターゲインの個別設定
        var gain = ImGui.SliderFloat("Master gain index", audio_get_master_gain(listener_index),0,1);
        audio_set_master_gain(listener_index, gain);
        // オーディオデバッグモード
        is_debug = ImGui.Checkbox("audio_debug_mode", is_debug);
        audio_debug(is_debug);
        // 全てのサウンドの停止
        if(ImGui.Button("stop all")){
            audio_stop_all();
            // オーディオデータの停止状態の更新
            var itr = ds_map_find_first(audio_data);
            var size = ds_map_size(audio_data);
            for(var map_i = 0; map_i < size; map_i ++){
            	var data = ds_map_find_value(audio_data, itr);
            	data.is_loop = false;
                itr = ds_map_find_next(audio_data, itr);
            }
        }
    }
    /**
	 * サウンドアセットメニューを描画する関数
	 * @function
	 */
    static DrawAssetsMenu = function(){
        // フィルターテキストの入力
        filter_text = ImGui.InputText("filter", filter_text);
        ImGui.SameLine();
        if(ImGui.Button("filter set")){
            // オーディオデータのフィルタリング
            var count = 0;
            name_array = array_create(0);
            while(audio_exists(count)){
                var name = audio_get_name(count);
                if(filter_text != ""){
            		if(string_pos(filter_text, name) != 0){
            			array_push(name_array, name);
            		}
            	} else {
            	    array_push(name_array, name);
            	}
                count ++;
            }
            current_select_index = 0;
        }
        // アセットが無ければデバッグメニューも表示しない
        if(array_length(name_array) == 0){
            ImGui.Text("no exists");
            return ;
        }
        // サウンドアセットの選択
        if(ImGui.BeginCombo("audio_asset", name_array[current_select_index], false)){
            for(var i = 0; i < array_length(name_array); i++){
                var is_select = current_select_index == i;
                if(ImGui.Selectable(name_array[i], is_select)){
                    current_select_index = i;
                }
            }
            ImGui.EndCombo();
        }
        // パラメータ設定
        if(ImGui.CollapsingHeader("play_param")){
            DrawStructMenu(playParam);
        }
        // 選択されたサウンドのパラメータ
        var current_param = ds_map_find_value(audio_data, name_array[current_select_index]);
        // サウンドがループ再生中かどうかの表示
        if(audio_is_playing(current_param.sound_index) && current_param.is_loop){
            ImGui.Text("is playing loop");
        } else {
            // 一度再生およびループ再生ボタン
            if(ImGui.Button("play once")){
                current_param.PlayOnce(playParam);
            }
            if(ImGui.Button("play loop")){
                current_param.PlayLoop(playParam);
            }
        }
        // 停止ボタン
        if(ImGui.Button("stop")){
            current_param.StopSound();
        }
    }
}
/**
 * サウンドの再生パラメータを管理するクラス
 * @class
 * @param {number} _sound_index - サウンドのインデックス
 */
function SoundParam(_sound_index) constructor {
    sound_index = _sound_index;
    is_loop = false;
    /**
	 * 一度再生する関数
	 * @function
	 * @param {object} _playParam - 再生パラメータ
	 */
    static PlayOnce = function(_playParam){
        is_loop = false;
        audio_play_sound(sound_index, _playParam.priotiry,false,_playParam.gain,_playParam.offset,_playParam.pitch);
    }
    /**
	 * ループ再生する関数
	 * @function
	 * @param {object} _playParam - 再生パラメータ
	 */
    static PlayLoop = function(_playParam){
        is_loop = true;
        audio_play_sound(sound_index, _playParam.priotiry,true,_playParam.gain,_playParam.offset,_playParam.pitch);
    }
    /**
	 * サウンドを停止する関数
	 * @function
	 */
    static StopSound = function(){
        is_loop = false;
        audio_stop_sound(sound_index);
    }
}
