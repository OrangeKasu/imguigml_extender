/**
 * 文字列比較関数
 * @function
 * @param {string} a - 比較1
 * @param {string} b - 比較2
 * @returns {number} - a < b なら -1 a > b なら 1 a == b なら 0
 */
function string_compare(a, b) {
    var lenA = string_length(a);
    var lenB = string_length(b);
    var minLength = min(lenA, lenB);

    for (var i = 0; i < minLength; i++)
    {
        var charA = ord(string_char_at(a, i));
        var charB = ord(string_char_at(b, i));

        if (charA < charB)
        {
            return -1; // 文字列 a が文字列 b よりも前
        }
        else if (charA > charB)
        {
            return 1;  // 文字列 a が文字列 b よりも後
        }
    }

    // 共通部分が同じ場合、長い方の文字列が後
    if (lenA < lenB)
        return -1;
    else if (lenA > lenB)
        return 1;

    // 文字列が完全に一致
    return 0;
}