/**
 * RoomAssetsWindow roomデータを管理し、ウィンドウとして表示するためのクラスです。
 * @class
 * @extends ImGuiWindowBase
 * @param {string} _name - ウィンドウの名前
 */
function RoomAssetsWindow(_name) : ImGuiWindowBase(_name) constructor {
    data_map = ds_map_create();
    name_array = array_create(0);
    current_room_index = 0;
    
    var count = 0;
    var itr = room_first;
    while(room_exists(itr)){
        var name = room_get_name(itr);
        ds_map_add(data_map, name, itr);
        itr = room_next(itr);
        array_push(name_array, name);
    }
    /**
     * ウィンドウを描画する関数
     * @function
     */
    static DrawWindow = function(){
        DrawRoomInfo();
        if(ImGui.BeginCombo("room_asset", name_array[current_room_index], false)){
            for(var i = 0; i < array_length(name_array); i++){
                var is_select = current_room_index == ds_map_find_value(data_map, name_array[i]);
                if(ImGui.Selectable(name_array[i], is_select)){
                    current_room_index = ds_map_find_value(data_map, name_array[i]);
                }
            }
            ImGui.EndCombo();
        }
        
        if(ImGui.Button("go to room")){
            room_goto(current_room_index);
        }
    }
    /**
     * 部屋の情報を描画する関数
     * @function
     */
    static DrawRoomInfo = function(){
        var room_index = room;
        ImGui.Text(room_get_name(room_index));
        room_height = ImGui.DragInt("room_height", room_height, 1,0,1920);
        room_width = ImGui.DragInt("room_width", room_width, 1,0,1920);
        room_persistent = ImGui.Checkbox("room_persistent", room_persistent);
    }
}