/**
 * @classdesc ImGuiを使用してspriteの情報を表示するウィンドウを管理するクラス
 * @class
 * @extends ImGuiLayerInfoWindowBase
 * @param {string} _name - ウィンドウの名前
 * @param {number} _layer_id - ウィンドウの所属するレイヤーのID
 * @param {number} _sprite_element_id - 管理するspriteのID
 */
function SpriteImGui(_name,_layer_id, _sprite_element_id) : ImGuiLayerInfoWindowBase(_name, _layer_id) constructor {
    sprite_element_id = _sprite_element_id;
    sprite_surf = -1;
    is_destroy_popup = false;
    open_focus_color = c_aqua;
    open_not_focus_color = c_white;
    /**
	 * ウィンドウを描画する関数
	 * @function
	 */
    static DrawWindow = function(){
        if(layer_sprite_exists(layer_id, sprite_element_id) == false){
            ImGui.Text("sprite asset is not exists");
            return ;
        }
        
        if(ImGui.BeginTabBar("sprite info tab")){
            if(ImGui.BeginTabItem("basic info")){
                DrawBasicInfo();
                ImGui.EndTabItem();
            }
            if (ImGui.BeginTabItem("parameter info")) {
            	DrawSpriteParameter();
            	ImGui.EndTabItem();
            }
            if (ImGui.BeginTabItem("sprite image")) {
                DrawSpriteImage();
            	ImGui.EndTabItem();
            }
            ImGui.EndTabBar();
        }
        
        if(is_destroy_popup){
			DrawPopupModalWindow();
		}
    }
    /**
	 * spriteの基本情報タブを描画する関数
	 * @function
	 */
    static DrawBasicInfo = function(){
        if(ImGui.Button("Destroy")){
	        is_destroy_popup = true;
	    }
    }
    
    /**
	 * ポップアップモーダルウィンドウを描画する関数
	 * @function
	 */
    static DrawPopupModalWindow = function(){
    	var menu_str = "Caution!!";
		ImGui.OpenPopup(menu_str);
		ImGui.SetNextWindowPos(window_get_width() / 2, window_get_height () / 2, ImGuiCond.Appearing, 0.5, 0.5);
		if(ImGui.BeginPopupModal(menu_str,undefined, ImGuiWindowFlags.NoResize)){
			ImGui.Separator();
			ImGui.Text("Are you sure you want to destroy this sprite asset?");
			if (ImGui.Button("yes")) {
				if(layer_sprite_exists(layer_id, sprite_element_id)){
                    layer_sprite_destroy(sprite_element_id);
                }
			}
			ImGui.SameLine();
			if (ImGui.Button("no")) {
				ImGui.CloseCurrentPopup();
				is_destroy_popup = false;
			}
			ImGui.EndPopup();	
		}
    }
    /**
	 * スプライトのパラメータ情報タブを描画する関数
	 * @function
	 */
    static DrawSpriteParameter = function(){
        var spr = layer_sprite_get_sprite(sprite_element_id);
        ImGui.Text(string("sprite_index: {0}", sprite_get_name(spr)));
        
        var img_x = layer_sprite_get_x(sprite_element_id);
        img_x = ImGui.DragFloat("image_x", img_x, 0.1,0,10000);
        layer_sprite_x(sprite_element_id, img_x);
        
        var img_y = layer_sprite_get_y(sprite_element_id);
        img_y = ImGui.DragFloat("image_y", img_y, 0.1,0,10000);
        layer_sprite_y(sprite_element_id, img_y);
        
        var img_index = layer_sprite_get_index(sprite_element_id);
        img_index = ImGui.DragFloat("image_index", img_index, 0.1,0,10000);
        layer_sprite_index(sprite_element_id, img_index);
        
        var img_speed = layer_sprite_get_speed(sprite_element_id);
        img_speed = ImGui.DragFloat("image_speed", img_speed, 0.01,0,10);
        layer_sprite_speed(sprite_element_id, img_speed);
        
        var img_xscale = layer_sprite_get_xscale(sprite_element_id);
        img_xscale = ImGui.DragFloat("image_xscale", img_xscale, 0.01,-10,10);
        layer_sprite_xscale(sprite_element_id, img_xscale);
        
        var img_yscale = layer_sprite_get_yscale(sprite_element_id);
        img_yscale = ImGui.DragFloat("image_yscale", img_yscale, 0.01,-10,10);
        layer_sprite_yscale(sprite_element_id, img_yscale);
        
        var img_angle = layer_sprite_get_angle(sprite_element_id);
        img_angle = ImGui.DragFloat("image_angle", img_angle, 1,-360,360);
        layer_sprite_angle(sprite_element_id, img_angle);
        
        ImGui.Separator();
	    if(ImGui.CollapsingHeader("color setting")){
	        var color = layer_sprite_get_blend(sprite_element_id);
	        var new_color = ImGui.ColorPicker3("ImGui::ColorPicker3", color);
	        layer_sprite_blend(sprite_element_id, make_color_rgb(color_get_red(new_color),color_get_green(new_color),color_get_blue(new_color)));
	        var alpha = layer_sprite_get_alpha(sprite_element_id);
	        alpha = ImGui.DragFloat("image_alpha", alpha, 0.01,0,1);
	        layer_sprite_alpha(sprite_element_id, alpha);
	    }
	    ImGui.Separator();
    }
    /**
	 * スプライト画像を描画する関数
	 * @function
	 */
    static DrawSpriteImage = function(){
        var spr = layer_sprite_get_sprite(sprite_element_id);
        var img_index = layer_sprite_get_index(sprite_element_id);
        var img_xscale = layer_sprite_get_xscale(sprite_element_id);
        var img_yscale = layer_sprite_get_yscale(sprite_element_id);
        var img_angle = layer_sprite_get_angle(sprite_element_id);
        var color = layer_sprite_get_blend(sprite_element_id);
        var alpha = layer_sprite_get_alpha(sprite_element_id);
        
        if (!surface_exists(sprite_surf)) {
	        sprite_surf = surface_create(sprite_get_width(spr) + 1, sprite_get_height(spr) + 1);
	        surface_set_target(sprite_surf);
	        draw_sprite_ext(spr,img_index,0,0,img_xscale,img_yscale,img_angle,color,alpha);
	        surface_reset_target();
	    }
	    ImGui.BeginChild("sprite_image", 0, 0, true, ImGuiWindowFlags.HorizontalScrollbar);
	    ImGui.Text("sprite_image");
	    surface_set_target(sprite_surf);
	    draw_clear_alpha(c_white,0);
	    draw_sprite_ext(spr,img_index,sprite_get_xoffset(spr) * img_xscale,sprite_get_yoffset(spr) * img_yscale,img_xscale,img_yscale,img_angle,color,alpha);
	    surface_reset_target();
	    ImGui.Surface(sprite_surf);
	    ImGui.EndChild();
    }
    /**
	 * DrawEndEventで呼ばれる関数
	 * @function
	 */
    static DrawEndEvent = function(){
        if(layer_sprite_exists(layer_id, sprite_element_id)){
			var spr = layer_sprite_get_sprite(sprite_element_id);
			var img_x = layer_sprite_get_x(sprite_element_id);
            var img_y = layer_sprite_get_y(sprite_element_id);
            var img_xscale = layer_sprite_get_xscale(sprite_element_id);
            var img_yscale = layer_sprite_get_yscale(sprite_element_id);
			DrawFocusRectangle(spr, img_x,img_y,img_xscale,img_yscale);
		}
    }
}
/**
 * レイヤー内のスプライトをImGuiで表示する関数
 * @function
 * @param {object} _spriteMap - レイヤー内のスプライトを格納したマップ
 * @param {number} _layer_id - レイヤーのID
 */
function view_sprite_imguigml(_spriteMap, _layer_id) {
    var itr = ds_map_find_first(_spriteMap);
    var size = ds_map_size(_spriteMap);
    for(var map_i = 0; map_i < size; map_i ++){
        if(ImGui.TreeNode(itr)){
            var array = ds_map_find_value(_spriteMap, itr);
            for(var array_i = 0; array_i < array_length(array); array_i ++){
                var spr = layer_sprite_get_sprite(array[array_i]);
                var sprName = sprite_get_name(spr);
                
                var name = string("{0}_{1}", sprName,array[array_i]);
                if(ImGui.Button(name)){
                	if(instance_exists(obj_imguigml_manager)){
                		obj_imguigml_manager.GetDataMap().AddSprite(name, _layer_id, array[array_i]);
                	}
                }
            }
            ImGui.TreePop();
        }
        itr = ds_map_find_next(_spriteMap, itr);
    }
}

/**
 * スプライトをマップに追加する関数
 * @function
 * @param {number} _element - 追加するスプライトの要素
 * @param {object} _mapPtr - マップへのポインタ
 */
function add_sprite_map(_element, _mapPtr){
    var spr = layer_sprite_get_sprite(_element);
    var sprName = sprite_get_name(spr);
	var isRegist = ds_map_exists(_mapPtr, sprName);
	if(isRegist){
	    var array = ds_map_find_value(_mapPtr, sprName);
	    array_push(array,_element);
	} else {
	    var array = array_create(0);
	    array_push(array,_element);
	    ds_map_add(_mapPtr, sprName, array);
	}
}