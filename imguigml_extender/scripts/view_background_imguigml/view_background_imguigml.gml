/**
 * @classdesc ImGuiを使用して背景の情報を表示するウィンドウを管理するクラス
 * @class
 * @extends ImGuiLayerInfoWindowBase
 * @param {string} _name - ウィンドウの名前
 * @param {number} _layer_id - ウィンドウの所属するレイヤーのID
 * @param {number} _background_id - 管理する背景のID
 */
function BackgroundImGui(_name, _layer_id, _background_id) : ImGuiLayerInfoWindowBase(_name, _layer_id) constructor {
    background_id = _background_id;
    background_surf = -1;
    bg_view_scale = 1;
	    
	/**
	 * @function DrawWindow
	 * @brief タブバー内にレイヤー情報と背景情報のタブを描画する関数。
	 * @details ImGuiを使用して、レイヤー情報と背景情報のタブを描画します。
	 */
	static DrawWindow = function(){
	    if(ImGui.BeginTabBar("background_layer info tab")){
	        if (ImGui.BeginTabItem("layer info")) {
	            DrawLayerInfo();
	            ImGui.EndTabItem();
	        }
	        if (ImGui.BeginTabItem("background info")) {
	            DrawBackgroundWindow();
	            ImGui.EndTabItem();
	        }
	        ImGui.EndTabBar();
	    }
	}
	
	/**
	 * @function DrawBackgroundWindow
	 * @brief 背景情報を描画する関数。
	 * @details 背景の可視性やタイリング、色設定などをImGuiを使用して描画します。
	 */
	static DrawBackgroundWindow = function(){
	    var sprite = layer_background_get_sprite(background_id);
		ImGui.Text(sprite_get_name(sprite));
		var is_visible = ImGui.Checkbox("visible", layer_background_get_visible(background_id));
		layer_background_visible(background_id, is_visible);
		var is_htiled = ImGui.Checkbox("htiled", layer_background_get_htiled(background_id));
		layer_background_htiled(background_id, is_htiled);
		var is_vtiled = ImGui.Checkbox("vtiled", layer_background_get_vtiled(background_id));
		layer_background_vtiled(background_id, is_vtiled);
		var is_stretch = ImGui.Checkbox("stretch", layer_background_get_stretch(background_id));
		layer_background_stretch(background_id, is_stretch);
		var bg_speed = layer_background_get_speed(background_id);
		bg_speed = ImGui.DragFloat("speed", bg_speed, 0.01, 0, 10);
		layer_background_speed(background_id, bg_speed);
		
		var xscale = layer_background_get_xscale(background_id);
		xscale = ImGui.DragFloat("xscale", xscale, 0.01, 0, 10);
		layer_background_xscale(background_id, xscale);
		
		var yscale = layer_background_get_yscale(background_id);
		yscale = ImGui.DragFloat("yscale", yscale, 0.01, 0, 10);
		layer_background_yscale(background_id, yscale);
		ImGui.Separator();
		if (ImGui.CollapsingHeader("color setting")) {
		    var color = layer_background_get_blend(background_id);
		    var new_color = ImGui.ColorPicker3("ImGui::ColorPicker3", color);
		    layer_background_blend(background_id, make_color_rgb(color_get_red(new_color), color_get_green(new_color), color_get_blue(new_color)));
		    var alpha = layer_background_get_alpha(background_id);
		    alpha = ImGui.DragFloat("alpha", alpha, 0.01, 0, 1);
		    layer_background_alpha(background_id, alpha);
		}
		ImGui.Separator();
		DrawBackgroundImage(sprite);
	}
	
	/**
	 * @function DrawBackgroundImage
	 * @brief 背景画像を描画する関数。
	 * @details 背景用サーフェスを作成し、スプライトを描画して表示します。
	 * @param {Number} _sprite_index - 背景に使用するスプライトのインデックス。
	 */
	static DrawBackgroundImage = function(_sprite_index){
	    var index = layer_background_get_index(background_id);
	    
	    var xscale = layer_background_get_xscale(background_id) * bg_view_scale;
	    var yscale = layer_background_get_yscale(background_id) * bg_view_scale;
	    if (!surface_exists(background_surf)) {
	        background_surf = surface_create(sprite_get_width(_sprite_index) + 1, sprite_get_height(_sprite_index) + 1);
	        surface_set_target(background_surf);
	        draw_sprite_ext(_sprite_index,index,0,0,xscale,yscale,0,layer_background_get_blend(background_id),layer_background_get_alpha(background_id));
	        surface_reset_target();
	    }
	    ImGui.BeginChild("sprite_image", 0, 0, true, ImGuiWindowFlags.HorizontalScrollbar);
	    bg_view_scale = ImGui.SliderFloat("view_scale", bg_view_scale, 0,10);
	    ImGui.Text("sprite_image");
	    surface_set_target(background_surf);
	    draw_clear_alpha(c_white,0);
	    draw_sprite_ext(_sprite_index,index,0,0,xscale,yscale,0,layer_background_get_blend(background_id),layer_background_get_alpha(background_id));
	    surface_reset_target();
	    ImGui.Surface(background_surf);
	    ImGui.EndChild();
	}

}
/**
 * 背景を表示する関数
 * @function
 * @param {number} _layer_id - レイヤーのID
 */
function view_background_imguigml(_layer_id) {
    var background_id = layer_background_get_id(_layer_id);
    var name = layer_get_name(_layer_id);
    
    if(ImGui.Button(string("{0} open window", name))){
		obj_imguigml_manager.GetDataMap().AddBackground(name,_layer_id, background_id);
	}
}