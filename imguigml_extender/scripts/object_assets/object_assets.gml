/**
 * ImGuiを使用してオブジェクトの情報を表示するウィンドウを管理するクラス
 * @class
 * @extends ImGuiWindowBase
 * @param {string} _name - ウィンドウの名前
 */
function ObjectAssetsWindow(_name) : ImGuiWindowBase(_name) constructor {
    data_map = ds_map_create();
    var count = 0;
    while(true){
        var is_exist = object_exists(count);
        if(is_exist){
            var name = object_get_name(count);
            ds_map_add(data_map, name,count);
        } else {
            break;
        }
        count ++;
    }
    filter_str = "";
    name_array = Getname_array();
    data_array = Getdata_array();
    current_item = GetDefaultItem();
    current_object = GetDefaultObject();
    sprite_surf = -1;
    is_create_mode = false;
    is_draw_grid = false;
    grid_x = 32;
    grid_y = 32;
    is_grid = true;
    layer_array = layer_get_all();
    current_layer = layer_array[0];
    layer_name_array = array_create(0);
    for(var i = 0; i < array_length(layer_array); i ++){
    	array_push(layer_name_array, layer_get_name(layer_array[i]));
    }
    
    current_room = room;
    /**
     * オブジェクトの名前の配列を取得する関数
     * @returns {array} - オブジェクトの名前の配列
     */
    static Getname_array = function(){
        var array = array_create(0);
        var itr = ds_map_find_first(data_map);
        var size = ds_map_size(data_map);
        for(var map_i = 0; map_i < size; map_i ++){
        	if(filter_str != ""){
        		if(string_pos(filter_str, itr) != 0){
        			array_push(array, itr);
        		}
        		itr = ds_map_find_next(data_map, itr);
        		continue;
        	}
            array_push(array, itr);
            itr = ds_map_find_next(data_map, itr);
        }
        return array;
    }
    /**
     * オブジェクトのデータの配列を取得する関数
     * @returns {array} - オブジェクトのデータの配列
     */
    static Getdata_array = function(){
    	var array = array_create(0);
        var itr = ds_map_find_first(data_map);
        var size = ds_map_size(data_map);
        for(var map_i = 0; map_i < size; map_i ++){
        	var data = ds_map_find_value(data_map, itr);
        	if(filter_str != ""){
        		if(string_pos(filter_str, itr) != 0){
        			array_push(array, data);
        		}
        		itr = ds_map_find_next(data_map, itr);
        		continue;
        	}
        	
            array_push(array, data);
            itr = ds_map_find_next(data_map, itr);
        }
        return array;
    }
    /**
     * デフォルトのアイテムを取得する関数
     * @returns {string} - デフォルトのアイテム
     */
    static GetDefaultItem = function(){
    	if(array_length(name_array) > 0){
    		return name_array[0];
    	}
    	return "no exists";
    }
    /**
     * デフォルトのオブジェクトを取得する関数
     * @returns {object} - デフォルトのオブジェクト
     */
    static GetDefaultObject = function(){
    	if(array_length(data_array) > 0){
    		return data_array[0];
    	}
    	return noone;
    }
    /**
     * レイヤーのデータを更新する関数
     */
    static UpdateLayerData = function(){
    	layer_array = layer_get_all();
	    current_layer = layer_array[0];
	    layer_name_array = array_create(0);
	    for(var i = 0; i < array_length(layer_array); i ++){
	    	array_push(layer_name_array, layer_get_name(layer_array[i]));
	    }
    }
    /**
     * ウィンドウを描画する関数
     */
    static DrawWindow = function(){
    	// roomが切り替わったらレイヤーデータを更新する
    	if(current_room != room){
    		UpdateLayerData();
    		current_room = room;
    	}
    	filter_str = ImGui.InputText("filter", filter_str);
    	if(ImGui.Button("filter set")){
    		name_array = Getname_array();
    		data_array = Getdata_array();
    		current_item = GetDefaultItem();
    		current_object = GetDefaultObject();
    	}
    	ImGui.Separator();
        if(ImGui.BeginCombo("objectAsset", current_item, false)){
            for(var i = 0; i < array_length(name_array); i++){
                var is_select = current_item == name_array[i];
                if(ImGui.Selectable(name_array[i], is_select)){
                    current_item = name_array[i];
                    current_object = ds_map_find_value(data_map, current_item);
                }
            }
            ImGui.EndCombo();
        }
        ImGui.Separator();
        if(current_object != noone){
        	if(ImGui.CollapsingHeader("Create setting")){
	        	grid_x = ImGui.DragInt("grid_x", grid_x, 1,0,512,grid_x);
	        	grid_y = ImGui.DragInt("grid_y", grid_y, 1,0,512,grid_y);
	        	is_grid = ImGui.Checkbox("is_grid", is_grid);
	        	is_draw_grid = ImGui.Checkbox("is_draw_grid", is_draw_grid);
	        	var name = layer_get_name(current_layer);
	        	if(ImGui.BeginCombo("create layer", name, false)){
		            for(var i = 0; i < array_length(layer_array); i++){
		                var is_select = current_layer == layer_array[i];
		                if(ImGui.Selectable(layer_name_array[i], is_select)){
		                    current_layer = layer_array[i];
		                }
		            }
		            ImGui.EndCombo();
		        }
	        	if(ImGui.Button("Create Start")){
		        	is_collapsed = true;
		        	is_create_mode = true;
		        }
	        }
	        ImGui.Separator();
            DrawObjectInfo();
        }
    }
    /**
     * DrawEndEvent呼び出し関数
     */
    static DrawEndEvent = function(){
    	if(is_draw_grid){
    		draw_set_alpha(0.75);
    		for(var i = grid_x; i < room_width; i += grid_x){
    			draw_line_color(i,0,i,room_height,c_fuchsia,c_fuchsia);
    		}
    		for(var i = grid_y; i < room_height; i += grid_y){
    			draw_line_color(0,i,room_width,i,c_fuchsia,c_fuchsia);
    		}
    		draw_set_alpha(1);
    	}
    	if(!is_create_mode){
    		return ;
    	}
    	var pos_x = mouse_x;
		var pos_y = mouse_y;
		if(is_grid){
			pos_x = ConvertGridValue(pos_x,grid_x);
			pos_y = ConvertGridValue(pos_y,grid_y);
		}
    	if(mouse_check_button_pressed(mb_left)){
    		
			instance_create_layer(pos_x,pos_y,current_layer,current_object);
			is_create_mode = false;
			is_collapsed = false;
		}
		if(mouse_check_button_pressed(mb_right)){
			is_create_mode = false;
			is_collapsed = false;
		}
    	var spr = object_get_sprite(current_object);
        if(spr == -1){
            return ;
        }
    	draw_sprite(spr, 0, pos_x,pos_y);
    }
    /**
     * オブジェクト情報を描画する関数
     */
    static DrawObjectInfo = function(){
        var spr = object_get_sprite(current_object);
        if(spr == -1){
            return ;
        }
        if (!surface_exists(sprite_surf)) {
        	sprite_surf = surface_create(sprite_get_width(spr) + 1, sprite_get_height(spr) + 1);
        	surface_set_target(sprite_surf);
        	DrawSprite();
        	surface_reset_target();
        }
    	ImGui.BeginChild("sprite_image", 0, 0, true);
        surface_resize(sprite_surf,sprite_get_width(spr) + 1, sprite_get_height(spr) + 1);
        surface_set_target(sprite_surf);
        draw_clear_alpha(c_white,0.3);
    	DrawSprite();
    	surface_reset_target();
        ImGui.Surface(sprite_surf);
        ImGui.EndChild();
    }
    /**
	 * オブジェクトのスプライトを描画する関数
	 * @function
	 */
    static DrawSprite = function(){
        var spr = object_get_sprite(current_object);
        if(spr == -1){
            return ;
        }
    	draw_sprite(spr, 0, sprite_get_xoffset(spr), sprite_get_yoffset(spr));
    }
    /**
     * グリッドの値を変換する関数
     * @param {number} value - 変換する値
     * @param {number} grid - グリッドのサイズ
     * @returns {number} - 変換後の値
     */
    static ConvertGridValue = function(value,grid){
        var times = floor(value / grid);
        return grid * times;
    }
}