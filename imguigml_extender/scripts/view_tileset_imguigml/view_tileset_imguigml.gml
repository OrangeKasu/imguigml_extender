/**
 * ImGuiを使用してタイルセットの情報を表示するウィンドウを管理するクラス
 * @class
 * @extends ImGuiLayerInfoWindowBase
 * @param {string} _name - ウィンドウの名前
 * @param {number} _layer_id - ウィンドウの所属するレイヤーのID
 * @param {number} _tileset_id - タイルセットのID
 * @param {number} _tileset - タイルセットのインデックス
 */
function TilesetImGui(_name,_layer_id, _tileset_id, _tileset) : ImGuiLayerInfoWindowBase(_name, _layer_id) constructor {
    tileset_id = _tileset_id;
    tileset = _tileset;
    tile_surf = -1;
    /**
     * ウィンドウを描画するメソッド
     * @function
     */
    static DrawWindow = function(){
        ImGui.Text("tileset info");
        var info = tileset_get_info(tileset);
        ImGui.Text(GetInfoText(info));
    	if (!surface_exists(tile_surf)) {
        	tile_surf = surface_create(room_width + 1, room_height + 1);
        	surface_set_target(tile_surf);
            draw_tilemap(tileset_id, 0, 0);
        	surface_reset_target();
        }
	    ImGui.BeginChild("Inner_Tree", 0, 0, false, ImGuiWindowFlags.HorizontalScrollbar);
        surface_set_target(tile_surf);
        draw_clear_alpha(c_white,0);
        draw_tilemap(tileset_id, 0, 0);
    	surface_reset_target();
        ImGui.Surface(tile_surf);
        ImGui.EndChild();
    }
}
/**
 * タイルセットを表示するImGuiウィンドウを開くための関数
 * @function
 * @param {number} _layer_id - タイルセットが所属するレイヤーのID
 */
function view_tileset_imguigml(_layer_id){
	var tileId = layer_tilemap_get_id(_layer_id);
	var tileset = tilemap_get_tileset(tileId);
	var tileName = tileset_get_name(tileset);
	if(ImGui.TreeNode(tileName)){
		if(ImGui.Button(string("{0} open window", tileName))){
			obj_imguigml_manager.GetDataMap().AddTileset(tileName, _layer_id, tileId, tileset);
		}
		ImGui.TreePop();
	}
}